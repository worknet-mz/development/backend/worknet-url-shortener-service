from dependency_injector.containers import copy
from dependency_injector.providers import Factory

from core.dependency.uow_container import UOWContainer
from modules.shortened_url.usecase.add_shortened_url.impl import (
    AddShortenedURLUseCase,
)
from modules.shortened_url.usecase.find_shortened_url_by_original_url.impl import (
    FindShortenedURLByOriginalURLUseCase,
)
from modules.shortened_url.usecase.get_original_url.impl import (
    FindOriginalURLUseCase,
)


@copy(UOWContainer)
class UseCaseContainer(UOWContainer):
    add_shortened_url_uc = Factory(
        AddShortenedURLUseCase,
        uow=UOWContainer.shortened_url_persistence_uow,
    )

    find_shortened_url_by_original_url_uc = Factory(
        FindShortenedURLByOriginalURLUseCase,
        uow=UOWContainer.shortened_url_query_uow,
    )

    find_original_url_uc = Factory(
        FindOriginalURLUseCase,
        uow=UOWContainer.shortened_url_query_uow,
    )
