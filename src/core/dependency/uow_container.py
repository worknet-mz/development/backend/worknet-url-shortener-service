from dependency_injector import providers
from dependency_injector.containers import copy

from core.dependency.base_container import BaseContainer
from modules.shortened_url.infrastructure.persistence.uow import (
    ShortenedURLPersistenceUnitOfWork,
)
from modules.shortened_url.infrastructure.query.uow import (
    ShortenedURLQueryUnitOfWork,
)


@copy(BaseContainer)
class UOWContainer(BaseContainer):
    shortened_url_persistence_uow = providers.Factory(
        ShortenedURLPersistenceUnitOfWork,
        engine=BaseContainer.db.provided.engine,
    )

    shortened_url_query_uow = providers.Factory(
        ShortenedURLQueryUnitOfWork,
        engine=BaseContainer.db.provided.engine,
    )
