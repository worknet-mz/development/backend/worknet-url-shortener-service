from dependency_injector.containers import copy

from core.dependency.use_case_container import UseCaseContainer


@copy(UseCaseContainer)
class Container(UseCaseContainer):
    ...
