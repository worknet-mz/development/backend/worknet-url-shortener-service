from pydantic import Field

from core.config.base import BaseSettings


class DatabaseSettings(BaseSettings):
    engine: str = Field("postgresql+asyncpg", env="DATABASE_ENGINE")
    host: str = Field(default="localhost", env="DATABASE_HOST")
    port: int = Field(default=15432, env="DATABASE_PORT")
    name: str = Field(default="shortener", env="DATABASE_NAME")
    username: str = Field(default="postgres", env="DATABASE_USER")
    password: str = Field(default="password", env="DATABASE_PASSWORD")
