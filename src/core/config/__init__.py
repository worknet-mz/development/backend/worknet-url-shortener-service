from core.config.base import BaseSettings
from core.config.database import DatabaseSettings


class ApplicationSettings(BaseSettings):
    db: DatabaseSettings = DatabaseSettings()
