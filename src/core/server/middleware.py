import time

from fastapi.logger import logger as fastapi_logger
from fastapi.requests import Request
from starlette.middleware.base import BaseHTTPMiddleware


class StructlogMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        request.state.start_time = time.time()
        response = await call_next(request)

        structlog_message = {
            "remote_addr": request.client.host,
            "remote_port": request.client.port,
            "request_method": request.method,
            "request_path": request.url.path,
            "status_code": response.status_code,
            "response_time": (time.time() - request.state.start_time) * 1000,
        }

        fastapi_logger.info("Request completed", structlog_message)

        return response
