from fastapi import APIRouter, FastAPI


def add_routers(app: FastAPI, routers: list[APIRouter]) -> None:
    for router in routers:
        app.include_router(router)
