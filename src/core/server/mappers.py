from modules.shortened_url.infrastructure.persistence.mapper import (
    start_mapper as start_shortened_url_mapper,
)


def start_mappers():
    start_shortened_url_mapper()
