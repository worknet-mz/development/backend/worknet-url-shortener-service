from fastapi import HTTPException, status


class BaseHTTPException(HTTPException):
    def __init__(self, message: str, status_code: int):
        self._message = message
        super().__init__(status_code=status_code, detail=message)

    def __str__(self):
        return self._message


class BadRequestException(BaseHTTPException):
    def __init__(self, message: str):
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, message=message)


class NotFoundException(BaseHTTPException):
    def __init__(self, message: str):
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, message=message)
