import logging
import os
import sys

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from loguru import logger
from sqlalchemy.orm import clear_mappers

from core.dependency.container import Container
from core.server.error_handlers import init_error_handler
from core.server.mappers import start_mappers
from core.server.routers import add_routers
from modules.shortened_url.usecase import router as shortened_url_router
from modules.shortened_url.usecase.add_shortened_url import (
    api as add_shortened_url_api,
)
from modules.shortened_url.usecase.find_shortened_url_by_original_url import (
    api as find_shortened_url_by_original_url_api,
)
from modules.shortened_url.usecase.get_original_url import (
    api as get_original_url_api,
)

logger.remove(0)
logger.add(sys.stdout, format="{level}: {time} | {message}", level=logging.INFO)


def create_application() -> FastAPI:
    container = Container()
    container.wire(
        modules=[
            find_shortened_url_by_original_url_api,
            add_shortened_url_api,
            get_original_url_api,
        ]
    )

    application = FastAPI(
        title="URL Shortener",
        version="0.0.1",
        openapi_tags=[{"name": "URL Shortener", "description": "URL Shortener Service"}],
        openapi_url=os.getenv("OPENAPI_URL", "/api/openapi.json"),
    )

    application.container = container
    db = container.db()

    add_routers(
        application,
        [
            shortened_url_router,
        ],
    )

    application.add_middleware(
        CORSMiddleware,
        allow_credentials=True,
    )

    init_error_handler(application)

    @application.on_event("startup")
    async def startup_event():
        """Execute when the application starts."""
        logger.info("Starting up...")
        await db.connect()

        start_mappers()

        logger.info("Application started successfully.")

    @application.on_event("shutdown")
    async def shutdown_event():
        """Execute when the application shutdowns."""
        clear_mappers()

        await db.disconnect()

        logger.info("Application shuts down.")

    return application


app = create_application()
