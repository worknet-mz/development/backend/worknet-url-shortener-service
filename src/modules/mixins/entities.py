import datetime
import uuid

from sqlalchemy import UUID, DateTime, func
from sqlalchemy.orm import Mapped, declared_attr, mapped_column


class IdUUIDMixin:
    @declared_attr
    def id(self) -> Mapped[str]:
        return mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, insert_default=uuid.uuid4)


class TimestampMixin:
    @declared_attr
    def created_at(self) -> Mapped[datetime]:
        return mapped_column(DateTime(timezone=True), nullable=False, server_default=func.now())

    @declared_attr
    def updated_at(self) -> Mapped[datetime]:
        return mapped_column(DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())
