from pymfdata.common.usecase import BaseUseCase
from pymfdata.rdb.transaction import async_transactional

from modules.shortened_url.domain.exceptions import (
    OriginalURLNotFoundException,
)
from modules.shortened_url.infrastructure.query.dto import (
    ShortenedURLWithOriginalURLDTO,
)
from modules.shortened_url.infrastructure.query.uow import (
    ShortenedURLQueryUnitOfWork,
)


class FindOriginalURLUseCase(BaseUseCase[ShortenedURLQueryUnitOfWork]):
    def __init__(self, uow):
        self._uow = uow

    @async_transactional()
    async def invoke(self, shortened_url: str) -> ShortenedURLWithOriginalURLDTO:
        result = self.uow.repository.find_by_col(shortened_url_link=shortened_url)
        if result is None:
            raise OriginalURLNotFoundException(shortened_url)
        return ShortenedURLWithOriginalURLDTO.model_validate(result)
