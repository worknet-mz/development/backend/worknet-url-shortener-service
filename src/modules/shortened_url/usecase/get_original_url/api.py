from dependency_injector.wiring import Provide, inject
from fastapi import Depends, Path, status

from core.dependency.container import Container
from modules.shortened_url.infrastructure.query.dto import (
    ShortenedURLWithOriginalURLDTO,
)
from modules.shortened_url.usecase import router
from modules.shortened_url.usecase.get_original_url.impl import (
    FindOriginalURLUseCase,
)


@router.get(
    path="/find-original-url/{shortened_url}",
    description="Find original URL",
    summary="Find original URL",
    response_model=ShortenedURLWithOriginalURLDTO,
    status_code=status.HTTP_200_OK,
)
@inject
async def find_shortened_url_by_original_url(
    shortened_url: str = Path(description="Shortened URL"),
    uc: FindOriginalURLUseCase = Depends(Provide[Container.find_original_url_uc]),
):
    return await uc.invoke(shortened_url)
