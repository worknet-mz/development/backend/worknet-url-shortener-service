from pymfdata.common.usecase import BaseUseCase
from pymfdata.rdb.transaction import async_transactional

from modules.shortened_url.domain.exceptions import (
    ShortenedURLNotFoundByOriginalURLException,
)
from modules.shortened_url.infrastructure.query.dto import ShortenedURLDTO
from modules.shortened_url.infrastructure.query.uow import (
    ShortenedURLQueryUnitOfWork,
)
from modules.shortened_url.usecase.find_shortened_url_by_original_url.command import (
    FindShortenedURLByOriginalURLCommand,
)


class FindShortenedURLByOriginalURLUseCase(BaseUseCase[ShortenedURLQueryUnitOfWork]):
    def __init__(self, uow):
        self._uow = uow

    @async_transactional()
    async def invoke(self, command: FindShortenedURLByOriginalURLCommand) -> ShortenedURLDTO:
        result = self.uow.repository.find_by_col(**command.model_dump())
        if result is None:
            raise ShortenedURLNotFoundByOriginalURLException(command)
        return ShortenedURLDTO.model_validate(result)
