from pydantic import BaseModel, Field


class FindShortenedURLByOriginalURLCommand(BaseModel):
    original_url_link: str = Field(description="Original URL", alias="original_url")

    model_config = {
        "from_attributes": True,
        "populate_by_name": True,
    }
