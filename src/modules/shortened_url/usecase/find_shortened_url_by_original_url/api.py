from dependency_injector.wiring import Provide, inject
from fastapi import Depends, status

from core.dependency.container import Container
from modules.shortened_url.infrastructure.query.dto import ShortenedURLDTO
from modules.shortened_url.usecase import router
from modules.shortened_url.usecase.find_shortened_url_by_original_url.command import (
    FindShortenedURLByOriginalURLCommand,
)
from modules.shortened_url.usecase.find_shortened_url_by_original_url.impl import (
    FindShortenedURLByOriginalURLUseCase,
)


@router.get(
    path="/find-by-original-url",
    description="Find shortened URL by original URL",
    summary="Find shortened URL by original URL",
    response_model=ShortenedURLDTO,
    status_code=status.HTTP_200_OK,
)
@inject
async def find_shortened_url_by_original_url(
    command: FindShortenedURLByOriginalURLCommand,
    uc: FindShortenedURLByOriginalURLUseCase = Depends(Provide[Container.find_shortened_url_by_original_url_uc]),
):
    return await uc.invoke(command)
