from pymfdata.common.usecase import BaseUseCase
from pymfdata.rdb.transaction import async_transactional

from modules.shortened_url.domain.aggregate.model import ShortenedURL
from modules.shortened_url.infrastructure.persistence.uow import (
    ShortenedURLPersistenceUnitOfWork,
)
from modules.shortened_url.usecase.add_shortened_url.command import (
    AddShortenedURLCommand,
)


class AddShortenedURLUseCase(BaseUseCase[ShortenedURLPersistenceUnitOfWork]):
    def __init__(self, uow: ShortenedURLPersistenceUnitOfWork):
        self._uow = uow

    @async_transactional()
    async def invoke(self, command: AddShortenedURLCommand):
        if not await self.uow.repository.is_exists(original_url_link=command.original_url_link):
            shortened_url = ShortenedURL.new_shortened_url(command)
            self.uow.repository.create(shortened_url)
