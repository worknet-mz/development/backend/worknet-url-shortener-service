from dependency_injector.wiring import Provide, inject
from fastapi import Depends, status

from core.dependency.container import Container
from modules.shortened_url.usecase import router
from modules.shortened_url.usecase.add_shortened_url.command import (
    AddShortenedURLCommand,
)
from modules.shortened_url.usecase.add_shortened_url.impl import (
    AddShortenedURLUseCase,
)


@router.post(
    path="/add",
    name="Add shortened URL",
    summary="Create shortened URL by Original URL",
    status_code=status.HTTP_201_CREATED,
)
@inject
async def add_shortened_url(
    command: AddShortenedURLCommand, uc: AddShortenedURLUseCase = Depends(Provide[Container.add_shortened_url_uc])
):
    await uc.invoke(command)
