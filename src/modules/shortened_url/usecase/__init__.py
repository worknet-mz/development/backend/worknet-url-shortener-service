from fastapi import APIRouter

router = APIRouter(prefix="/api/v1/shortened_url", tags=["shortened_url"])
