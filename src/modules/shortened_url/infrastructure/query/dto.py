from uuid import UUID

from pydantic import BaseModel, Field


class BaseShortenedURLDTO(BaseModel):
    id: UUID = Field(..., description="Shortened URL ID")
    shortened_url_link: str = Field(..., description="Shortened URL")

    model_config = {
        "from_attributes": True,
        "populate_by_name": True,
    }


class ShortenedURLDTO(BaseShortenedURLDTO):
    ...


class ShortenedURLWithOriginalURLDTO(ShortenedURLDTO):
    original_url_link: str = Field(..., description="Original URL")
