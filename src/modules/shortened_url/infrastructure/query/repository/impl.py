from pymfdata.rdb.repository import AsyncRepository, AsyncSession

from modules.shortened_url.domain.aggregate.model import ShortenedURL
from modules.shortened_url.infrastructure.query.repository.protocol import (
    ShortenedURLQueryRepository,
)


class ShortenedURLQueryAlchemyRepository(AsyncRepository[ShortenedURL, str], ShortenedURLQueryRepository):
    def __init__(self, session: AsyncSession):
        self._session = session
