from pymfdata.rdb.mapper import mapper_registry

from modules.shortened_url.domain.aggregate.model import ShortenedURL
from modules.shortened_url.infrastructure.persistence.entity import (
    ShortenedURLEntity,
)


def start_mapper():
    t = ShortenedURLEntity.__table__

    mapper_registry.map_imperatively(
        ShortenedURL,
        t,
    )
