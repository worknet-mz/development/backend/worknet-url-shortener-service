from pymfdata.rdb.connection import AsyncEngine
from pymfdata.rdb.usecase import AsyncSQLAlchemyUnitOfWork

from modules.shortened_url.infrastructure.persistence.repository.impl import (
    ShortenedURLPersistenceAlchemyRepository,
)


class ShortenedURLPersistenceUnitOfWork(AsyncSQLAlchemyUnitOfWork):
    def __init__(self, engine: AsyncEngine):
        super().__init__(engine)

    async def __aenter__(self):
        await super().__aenter__()

        self.repository = ShortenedURLPersistenceAlchemyRepository(self.session)
