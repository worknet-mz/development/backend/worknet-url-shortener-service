from pymfdata.rdb.connection import Base
from sqlalchemy import Index, String
from sqlalchemy.orm import Mapped, mapped_column

from modules.mixins.entities import IdUUIDMixin, TimestampMixin


class ShortenedURLEntity(Base, IdUUIDMixin, TimestampMixin):
    __tablename__ = "shortened_urls"

    __table_args__ = (
        Index("idx_shortened_url_shortened_url_link", "shortened_url_link"),
        Index("idx_shortened_url_original_url_link", "original_url_link"),
    )

    original_url_link: Mapped[str] = mapped_column(String, nullable=False, unique=True)
    shortened_url_link: Mapped[str] = mapped_column(String, nullable=False, unique=True)

    def __repr__(self) -> str:
        return self.shortened_url_link
