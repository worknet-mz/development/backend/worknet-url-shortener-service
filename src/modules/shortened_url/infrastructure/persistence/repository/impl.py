from pymfdata.rdb.repository import AsyncRepository, AsyncSession

from modules.shortened_url.domain.aggregate.model import ShortenedURL
from modules.shortened_url.infrastructure.persistence.repository.protocol import (
    ShortenedURLPersistenceRepository,
)


class ShortenedURLPersistenceAlchemyRepository(AsyncRepository[ShortenedURL, str], ShortenedURLPersistenceRepository):
    def __init__(self, session: AsyncSession) -> None:
        self._session = session
