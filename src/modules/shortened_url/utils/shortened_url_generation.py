import random
import string


def generate_shortened_url():
    return "".join(random.choice(string.ascii_letters) for _ in range(random.randint(4, 16)))
