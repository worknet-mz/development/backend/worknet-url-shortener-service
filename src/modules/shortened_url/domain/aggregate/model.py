from pydantic.dataclasses import dataclass

from modules.shortened_url.domain.aggregate.id import ShortenedURLId
from modules.shortened_url.usecase.add_shortened_url.command import (
    AddShortenedURLCommand,
)
from modules.shortened_url.utils import generate_shortened_url


@dataclass
class ShortenedURL:
    id: ShortenedURLId
    original_url_link: str
    shortened_url_link: str

    @staticmethod
    def new_shortened_url(command: AddShortenedURLCommand):
        return ShortenedURL(
            id=ShortenedURLId.next_id(),
            shortened_url_link=generate_shortened_url(),
            **command.model_dump(),
        )
