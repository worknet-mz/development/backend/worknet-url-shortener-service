from uuid import UUID, uuid4

from pydantic import UUID4
from pydantic.dataclasses import dataclass


@dataclass
class ShortenedURLId(UUID4):
    id: UUID

    @staticmethod
    def next_id() -> UUID:
        return uuid4()
