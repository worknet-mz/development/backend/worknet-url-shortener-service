from core.server.errors import NotFoundException
from modules.shortened_url.usecase.find_shortened_url_by_original_url.command import (
    FindShortenedURLByOriginalURLCommand,
)


class ShortenedURLNotFoundByOriginalURLException(NotFoundException):
    def __init__(self, command: FindShortenedURLByOriginalURLCommand):
        self._message = "Shortened URL not found by original URL {}".format(command.original_url_link)

        super().__init__(message=self._message)


class OriginalURLNotFoundException(NotFoundException):
    def __init__(self, shortened_url: str):
        self._message = "Original URL not found by shortened URL {}".format(shortened_url)

        super().__init__(message=self._message)
