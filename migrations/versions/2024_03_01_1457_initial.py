"""initial

Revision ID: 240301145729
Revises:
Create Date: 2024-03-01 14:57:29.660151

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "240301145729"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "shortened_urls",
        sa.Column("original_url_link", sa.String(), nullable=False),
        sa.Column("shortened_url_link", sa.String(), nullable=False),
        sa.Column("id", sa.UUID(), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.text("now()"), nullable=False),
        sa.Column("updated_at", sa.DateTime(timezone=True), server_default=sa.text("now()"), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("original_url_link"),
        sa.UniqueConstraint("shortened_url_link"),
    )
    op.create_index("idx_shortened_url_original_url_link", "shortened_urls", ["original_url_link"], unique=False)
    op.create_index("idx_shortened_url_shortened_url_link", "shortened_urls", ["shortened_url_link"], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index("idx_shortened_url_shortened_url_link", table_name="shortened_urls")
    op.drop_index("idx_shortened_url_original_url_link", table_name="shortened_urls")
    op.drop_table("shortened_urls")
    # ### end Alembic commands ###
